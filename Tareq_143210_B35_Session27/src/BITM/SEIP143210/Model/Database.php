<?php
namespace App\Model;

use PDO;
use PDOException;

class Database{

    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b35";
    public $username="root";
    public $password="";

    public function __construct()
    {

        try{

            $this->DBH = new PDO("mysql:host=localhost;dbname=atomic_project_b35", $this->username, $this->password);
            //echo "Sucessfully Connected Database";

        }
        catch(PDOException $e){
            //echo "Connected Fail";

            echo $e->getMessage();

        }

    }

}
$obj = new Database();