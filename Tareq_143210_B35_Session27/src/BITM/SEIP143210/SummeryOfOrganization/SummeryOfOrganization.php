<?php
namespace App\SummeryOfOrganization;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class SummeryOfOrganization extends DB{

    public $id;
    public $name="";
    public $summary="";

    public function __construct()
    {

        parent::__construct();

    }
    public function setData($data=NULL){

        if(array_key_exists('name',$data)){


            $this->name= $data['name'];

        }
        if(array_key_exists('summary',$data)){

            $this->summary = $data['summary'];

        }

    }
    public function store(){
        $arrData = array($this->name,$this->summary);
        $sql = "INSERT INTO  summary_of_organization(name,summary) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result=$STH->execute($arrData);

        if($result)
            Message::message("Data has been inserted successfully! :)");
        else
            Message::message("Your Data does not inserted. :(");

        Utility::redirect('create.php');

    }

    public function index(){


    }


}